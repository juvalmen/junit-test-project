package com.tecso.technicaltest.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.dto.PersonaDto;
import com.tecso.technicaltest.enums.CategoriaEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.repository.jdbc.JdbcPersonaRepository;
import com.tecso.technicaltest.service.impl.CategoriaServiceImpl;
import com.tecso.technicaltest.service.impl.PersonaServiceImpl;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@RunWith(SpringRunner.class)
@SpringBootTest

public class JPAHibernateCRUDTest {
	
	@InjectMocks
	@Autowired
	private PersonaServiceImpl personaServiceImpl;

	@Mock
	private JdbcPersonaRepository jdbcPersonaRepository;
	
	@Mock
	private CategoriaServiceImpl categoriaServiceImpl;
	
	@Mock
	@Autowired
	private DummyFieldMapper dummyFieldMapper;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
    @Test
    public void testGetObjectById_success() throws TecsoCustomException{
    	try {
			PersonaDto persona = new PersonaDto();
			persona.setIdpersona(3L);
			persona.setNombre("dasdsa");
			persona.setApellido("dasdsa");
			persona.setCuit("432432");
			persona.setDni("4dasdsa32432");
			persona.setRazonsocial("ythtgfdghgf");
			persona.setIdCategoriaPersona(1L);
			
			CategoriaDto categoriaDto = new CategoriaDto();
			categoriaDto.setIdcategoria(1L);
			categoriaDto.setNombre(CategoriaEnum.FISICA.getType());
			categoriaDto.setValor("1");
			categoriaDto.setLlave("categoriapersona");
		
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("ddd")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			personaServiceImpl.savePersona(persona);
			PersonaDto personaVo = personaServiceImpl.getPersonaById(3L);
		} catch (TecsoCustomException e) {
			throw new TecsoCustomException(e.getMessage());
		}
    }
/*
    @Test
    public void testGetAll_success() {
        List<Persona> personas = em.createNamedQuery("Persona.getAll", Persona.class).getResultList();
        assertEquals(1, personas.size());
    }

    @Test
    public void testPersist_success() {
    	
    	Persona persona = new Persona();
    	persona.setNombre("dasdsa");
    	persona.setApellido("dasdsa");
    	persona.setCuit("432432");
    	persona.setDni("4dasdsa32432");
    	persona.setRazonsocial("ythtgfdghgf");
    	
        em.getTransaction().begin();
        em.persist(persona);
        em.getTransaction().commit();

        List<Persona> personas = em.createNamedQuery("Persona.getAll", Persona.class).getResultList();

        assertNotNull(personas);
        assertEquals(2, personas.size());
    }

    @Test
    public void testDelete_success(){
        Persona persona = em.find(Persona.class, 1);

        em.getTransaction().begin();
        em.remove(persona);
        em.getTransaction().commit();

        List<Persona> personas = em.createNamedQuery("Persona.getAll", Persona.class).getResultList();

        assertEquals(0, personas.size());
    }
    */

}
