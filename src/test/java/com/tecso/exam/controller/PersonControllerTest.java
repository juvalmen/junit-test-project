//package com.tecso.exam.controller;
//
//import static org.junit.Assert.assertEquals;
//import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Collections;
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.json.JacksonTester;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.tecso.technicaltest.controller.PersonaController;
//import com.tecso.technicaltest.dto.PersonaDto;
//import com.tecso.technicaltest.dto.ResponseDto;
//import com.tecso.technicaltest.service.PersonaService;
//import com.tecso.technicaltest.util.SystemMessage;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(controllers = PersonaController.class)
//@AutoConfigureMockMvc(secure=false)
//public class PersonControllerTest {
//
//    private MockMvc mvc;
//
//	@MockBean
//	private PersonaService personService;
//	
//	@Autowired
//    private WebApplicationContext webApplicationContext;
//	
//	@Autowired private ObjectMapper objectMapper;
//	
//	private JacksonTester <Object> listJsonTester;
//	private PersonaDto person;
//	
//	@Autowired
//	private PersonaController controller;
//	
//	@Before
//    public void setup() {
//		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//        JacksonTester.initFields(this, objectMapper);
//        person = new PersonaDto();
//    	person.setIdperson(17L);
//    	person.setFullName("Oscar Mera");
//    	person.setCuit("1143");
//    	person.setDni("1143");
//    	person.setLastName("Mera");
//       	person.setName("Oscar");
//       	person.setIdTypePerson(1L);
//       	person.setValueTypePerson("10");
//       	person.setDescriptionTypePerson("Fisica");;
//    }
// 
//    @Test
//    public void findAll() throws Exception {
//    
//    	//Given
//    	List<PersonaDto> persons = Collections.singletonList(person);
//        given(personService.getAllPerson()).willReturn(persons);
//        final String outputJson = listJsonTester.write(persons).getJson();
//        
//        //When
//        String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
//        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get(root +"/getAll"))
//                .andExpect(status().isOk());
//        
//        MvcResult result = resultActions.andReturn();
//        String contentAsString = result.getResponse().getContentAsString();
//        ResponseDto response = objectMapper.readValue(contentAsString, ResponseDto.class);
//        
//        //Then
//        assertEquals(listJsonTester.write(response.getResponseBody()).getJson(), outputJson);
//        assertEquals(response.getResponseMessage(), SystemMessage.TRANSACCION_EXITOSA);
//    }
//    
//  
//
//}
