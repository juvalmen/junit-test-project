
CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL,
  `llave` varchar(100),
  `nombre` varchar(100),
  `valor` varchar(100),
  PRIMARY KEY (`idcategoria`)
);



CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(80),
  `apellido` varchar(250),
  `cuit` varchar(45),
  `dni` varchar(45),
  `razonsocial` varchar(100),
  `aniofundacion` int(6),
  `categoriapersona` int(6),  
  PRIMARY KEY (`idpersona`)
);


INSERT INTO categoria (idcategoria, llave, nombre, valor) 
VALUES (1,'categoriapersona','Física','1');
INSERT INTO categoria (idcategoria, llave, nombre, valor) 
VALUES (2,'categoriapersona','Jurídica','2');

INSERT INTO persona (idpersona, nombre, apellido, razonsocial, cuit, dni, aniofundacion, categoriapersona) 
VALUES(1, 'eqwewq','wqewqewq','eqweqwewqewq','eqweqwewqewq','eqweqwewqewq',1994, 1);