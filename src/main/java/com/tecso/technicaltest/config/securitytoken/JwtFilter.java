package com.tecso.technicaltest.config.securitytoken;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.GenericFilterBean;

import com.tecso.technicaltest.service.UserLoginSecurityService;

@Configuration
public class JwtFilter extends GenericFilterBean {

	@Autowired
    private com.tecso.technicaltest.service.TokenService tokenService;
	
	@Autowired
	private UserLoginSecurityService userLoginSecurityService;

	public JwtFilter(UserLoginSecurityService userLoginSecurityService) {
		this.userLoginSecurityService = userLoginSecurityService;
	}

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String token = request.getHeader("Authorization");

        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            response.sendError(HttpServletResponse.SC_OK, "success");
            return;
        }

        if (allowRequestWithoutToken(request)) {
            response.setStatus(HttpServletResponse.SC_OK);
            filterChain.doFilter(req, res);
        } else {
            if (token == null || !tokenService.isTokenValid(token)) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            } else {
            	String username = JwtTokenUtil.getUserNameFromToken(token);
            	
    			UserDetails userDetails = userLoginSecurityService.loadUserByUsernameAndToken(username, token);

    			if (JwtTokenUtil.validateToken(token, userDetails)) {
    				UsernamePasswordAuthenticationToken tokenObtained = new UsernamePasswordAuthenticationToken(userDetails,
    						userDetails.getPassword(), userDetails.getAuthorities());
    				SecurityContextHolder.getContext().setAuthentication(tokenObtained);
    			}
                filterChain.doFilter(req, res);

            }
        }

    }

    /**
     * 
     * @param request
     * @return
     */
    public boolean allowRequestWithoutToken(HttpServletRequest request) {
        if (request.getRequestURI().contains("/users/register") || request.getRequestURI().contains("/users/login")) {
            return true;
        }
        return false;
    }
}
