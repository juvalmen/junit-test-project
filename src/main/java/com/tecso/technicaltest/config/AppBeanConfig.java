package com.tecso.technicaltest.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;

@Configuration
@EnableWebMvc
@EnableConfigurationProperties
@ComponentScan(
		value = {
				"com.tecso.technicaltest.config.security",				
				"com.tecso.technicaltest.repository",
				"com.tecso.technicaltest.service",
				"com.tecso.technicaltest.mappers.orika",
				"com.tecso.technicaltest.controller"
				
		})
public class AppBeanConfig {

//	@Autowired
//	@Qualifier("dummyFieldMapper")
//	private DummyFieldMapper "dummyFieldMapper";
	
	 @Bean(name = "dummyFieldMapper")
	  public DummyFieldMapper getDummyFieldMapper() {
	    DummyFieldMapper dummyFieldMapper = new DummyFieldMapper();
	    return dummyFieldMapper;
	  }
//
//	@Bean
//	public void registerConfigurableMappers(DefaultMapperFactory mapperFactory) { 
//		dummyFieldMapper.configure(mapperFactory);
//	}

	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor() {
		return new MethodValidationPostProcessor();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
