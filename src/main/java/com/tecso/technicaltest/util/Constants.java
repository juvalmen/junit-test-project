package com.tecso.technicaltest.util;

public class Constants {
	public static final String TOKEN_KEY = "Authorization";
	public static final String LOGIN_URL = "/users/login";
	public static final String REGISTER_URL = "/users/register";
	
	private Constants() {
	}
}
