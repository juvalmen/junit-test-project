package com.tecso.technicaltest.service.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tecso.technicaltest.dto.UserLoginDto;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.User;
import com.tecso.technicaltest.repository.jpa.JpaUserLoginRepository;
import com.tecso.technicaltest.service.UserLoginService;
import com.tecso.technicaltest.util.SystemMessage;
import com.tecso.technicaltest.util.validations.SecurityValidationMessages;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@Service
public class UserLoginServiceImpl implements UserLoginService {

	private JpaUserLoginRepository jpaUserRepository;
	private DummyFieldMapper dummyFieldMapper;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	/**
	 * 
	 * @author Julian Valencia
	 * 20/05/2019
	 * @param jpaUserRepository
	 * @param dummyFieldMapper
	 * @param bCryptPasswordEncoder
	 */
	public UserLoginServiceImpl(JpaUserLoginRepository jpaUserRepository, DummyFieldMapper dummyFieldMapper,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.jpaUserRepository = jpaUserRepository;
		this.dummyFieldMapper = dummyFieldMapper;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	/**
	 * 
	 */
	@Override
	public UserLoginDto getUserByUserName(String userName, String token) throws TecsoCustomException {
		User user = jpaUserRepository.findUserByUsername(userName);
		if (user != null && user.getUsername() != null) {
			user.setToken(token);
			jpaUserRepository.save(user);
			UserLoginDto userDto = dummyFieldMapper.map(user, UserLoginDto.class);			
			userDto.setPassword(null);
			return userDto;
		} else {
			throw new TecsoCustomException(SecurityValidationMessages.USER_DOES_NOT_EXIST);
		}
	}
	
	/**
	 * 
	 */
	@Override
	public UserLoginDto getUserByUserName(String userName) throws TecsoCustomException {
		User user = jpaUserRepository.findUserByUsername(userName);
		if (user != null && user.getUsername() != null) {
			UserLoginDto userDto = dummyFieldMapper.map(user, UserLoginDto.class);			
			userDto.setPassword(null);
			return userDto;
		} else {
			throw new TecsoCustomException(SecurityValidationMessages.USER_DOES_NOT_EXIST);
		}
	}

	/**
	 * 
	 */
	@Transactional
	@Override
	public boolean deleteUserByUserName(String userName) throws TecsoCustomException {
		try {
			jpaUserRepository.delete(jpaUserRepository.getOne(userName));
			return Boolean.TRUE;
		} catch (EmptyResultDataAccessException e) {
			throw new TecsoCustomException(SecurityValidationMessages.USER_DOES_NOT_EXIST);
		} catch (Exception e) {
			throw new TecsoCustomException(SystemMessage.UNCONTROLED_ERROR);
		}

	}

	/**
	 * 
	 */
	@Override
	public void setUserSave(UserLoginDto userDTO) throws TecsoCustomException {
		try {
			jpaUserRepository.save(dummyFieldMapper.convert(userDTO, User.class, "DummyConverter"));
		} catch (EmptyResultDataAccessException e) {
			throw new TecsoCustomException(SecurityValidationMessages.USER_DOES_NOT_EXIST);
		} catch (Exception e) {
			throw new TecsoCustomException(SystemMessage.UNCONTROLED_ERROR);
		}
	}

	/**
	 * 
	 */
	@Transactional
	@Override
	public boolean setUserRegister(UserLoginDto userDto) throws TecsoCustomException {
		User user = jpaUserRepository.findUserByUsername(userDto.getUsername());

		if (user == null) {
			user = dummyFieldMapper.map(userDto, User.class);
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			user.setEnabled(Boolean.TRUE);
			jpaUserRepository.save(user);
		} else {
			StringBuilder messageError = new StringBuilder("El usuario ");
			messageError.append(userDto.getUsername());
			messageError.append(" ya existe");
			throw new TecsoCustomException(messageError.toString());
		}

		return true;

	}
}
