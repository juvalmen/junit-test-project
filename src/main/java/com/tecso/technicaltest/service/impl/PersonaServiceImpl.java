package com.tecso.technicaltest.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.dto.PersonaDto;
import com.tecso.technicaltest.enums.CategoriaEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Persona;
import com.tecso.technicaltest.repository.jdbc.JdbcPersonaRepository;
import com.tecso.technicaltest.repository.jpa.JpaPersonaRepository;
import com.tecso.technicaltest.service.CategoriaService;
import com.tecso.technicaltest.service.PersonaService;
import com.tecso.technicaltest.util.validations.PersonaValidationMessages;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@Service
public class PersonaServiceImpl implements PersonaService {

	private JpaPersonaRepository jpaPersonaRepository;
	private JdbcPersonaRepository jdbcPersonaRepository;
	private DummyFieldMapper dummyFieldMapper;
	private CategoriaService categoriaService;

	@Autowired
	public PersonaServiceImpl(JpaPersonaRepository jpaPersonaRepository, JdbcPersonaRepository jdbcPersonaRepository,
			DummyFieldMapper dummyFieldMapper,CategoriaService categoriaService) {
		this.jpaPersonaRepository = jpaPersonaRepository;
		this.jdbcPersonaRepository = jdbcPersonaRepository;
		this.dummyFieldMapper = dummyFieldMapper;
		this.categoriaService = categoriaService;
	}
	
	@Override
	public boolean savePersona(PersonaDto personaDto) throws TecsoCustomException {
		try {

			if (personaDto.getCuit() == null || personaDto.getCuit().isEmpty()) {
				throw new TecsoCustomException(PersonaValidationMessages.CUIT_NULO);
			}
			
			if (personaDto.getIdCategoriaPersona() == null) {
				throw new TecsoCustomException(PersonaValidationMessages.CATEGORIA_PERSONA_NULO);
			}
			
			if (verificarCuit(personaDto.getCuit())) {
				throw new TecsoCustomException(PersonaValidationMessages.CUIT_EXISTE);
			}

			CategoriaDto categoria = categoriaService.getCategoriaPorId(personaDto.getIdCategoriaPersona());
			
			if (categoria.getValor().equals(CategoriaEnum.FISICA.getType())) {
				validarPersonaCategoriaFisica(personaDto);
			} else if (categoria.getValor().equals(CategoriaEnum.JURIDICA.getType())) {
				validarPersonaCategoriaJuridica(personaDto);
			}

			Persona person = dummyFieldMapper.convert(personaDto, Persona.class, "DummyConverter");
			
			jpaPersonaRepository.save(person);
			
		} catch (Exception e) {
			throw new TecsoCustomException(e.getMessage());
		}
		return Boolean.TRUE;
	}

	@Override
	public List<PersonaDto> getPersonasList() throws TecsoCustomException {
		
		List<PersonaDto>  personas = jdbcPersonaRepository.getPersonasList();
		return personas;
	}

	

	@Override
	public boolean updatePersona(PersonaDto personaDto) throws TecsoCustomException {
		try {

			if (personaDto.getIdpersona() == null) {
				throw new TecsoCustomException(PersonaValidationMessages.ID_PERSONA_NO_EXISTE);
			}

			if (Strings.isNullOrEmpty(personaDto.getCuit())) {
				throw new TecsoCustomException(PersonaValidationMessages.CUIT_NULO);
			}

			CategoriaDto categoria = categoriaService.getCategoriaPorId(personaDto.getIdCategoriaPersona());
			if(categoria == null || categoria.getIdcategoria() == null) {
			  throw new TecsoCustomException(PersonaValidationMessages.CATEGORIA_PERSONA_NO_EXISTE);
			}
			
			PersonaDto personValidate = this.getPersonaById(personaDto.getIdpersona());
			if(personValidate == null || personValidate.getIdpersona() == null) {
				throw new TecsoCustomException(PersonaValidationMessages.ID_PERSONA_NO_EXISTE);
			}
			
			//si a cambiado el cuit se valida que no exista
			if(!personValidate.getCuit().equalsIgnoreCase(personaDto.getCuit()) && (verificarCuit(personaDto.getCuit()))) {
			   throw new TecsoCustomException(PersonaValidationMessages.CUIT_EXISTE);
			}

			if (categoria.getValor().equals(CategoriaEnum.FISICA.getType())) {
				validarPersonaCategoriaFisica(personaDto);
			} else if (categoria.getValor().equals(CategoriaEnum.JURIDICA.getType())) {
				validarPersonaCategoriaJuridica(personaDto);
			}
			
			Persona personaUdate = dummyFieldMapper.convert(personaDto, Persona.class, "DummyConverter");
	
			jpaPersonaRepository.save(personaUdate);
		} catch (Exception e) {
			throw new TecsoCustomException(e.getMessage());
		}
		return Boolean.TRUE;
	}

	@Override
	public boolean deletePersona(Long id) throws TecsoCustomException {
		try {
			jpaPersonaRepository.deleteById(id);
		} catch (Exception e) {
			throw new TecsoCustomException(e);
		}
		return Boolean.TRUE;
	}

	@Override
	public PersonaDto getPersonaById(Long id) throws TecsoCustomException {
		try {
			Optional<Persona> persona = jpaPersonaRepository.findById(id);
			if(persona.isPresent()) {
				return dummyFieldMapper.convert(persona.get(), PersonaDto.class, "DummyConverter");
			}
			return null;
		} catch (Exception e) {
			throw new TecsoCustomException(e);
		}
	}

	@Override
	public boolean verificarCuit(String cuit) throws TecsoCustomException {
		try {
			boolean cuitExist = false;
			List<PersonaDto> person = jdbcPersonaRepository.getPersonaPorIdentificacionCuit(cuit);
			if (person != null && !person.isEmpty()) {
				cuitExist = true;
			}
			return cuitExist;
		} catch (Exception e) {
			throw new TecsoCustomException(e);
		}
	}

	private void validarPersonaCategoriaJuridica(PersonaDto personaDto) throws TecsoCustomException {
		if (personaDto.getRazonsocial() == null || personaDto.getRazonsocial().isEmpty()) {
			throw new TecsoCustomException(PersonaValidationMessages.RAZON_SOCIAL_NULA);
		}
		if (personaDto.getRazonsocial().length() > 100) {
			throw new TecsoCustomException(PersonaValidationMessages.RAZON_SOCIAL_LONGITUD);
		}
		if (personaDto.getAniofundacion() == null) {
			throw new TecsoCustomException(PersonaValidationMessages.ANIO_DE_FUNDACION_OBLIGATORIO);
		}
	}

	private void validarPersonaCategoriaFisica(PersonaDto personaDto) throws TecsoCustomException {
		if (Strings.isNullOrEmpty(personaDto.getNombre())) {
			throw new TecsoCustomException(PersonaValidationMessages.NOMBRE_NULO);
		}
		if (personaDto.getNombre().length() > 80) {
			throw new TecsoCustomException(PersonaValidationMessages.NOMBRE_LONGITUD);
		}

		if (Strings.isNullOrEmpty(personaDto.getApellido())) {
			throw new TecsoCustomException(PersonaValidationMessages.APELLIDO_NULO);
		}
		if (personaDto.getApellido().length() > 250) {
			throw new TecsoCustomException(PersonaValidationMessages.APELLIDO_LONGITUD);
		}

		if (Strings.isNullOrEmpty(personaDto.getDni())) {
			throw new TecsoCustomException(PersonaValidationMessages.DNI_NULO);
		}
	}

}
