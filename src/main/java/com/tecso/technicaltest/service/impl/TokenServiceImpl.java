package com.tecso.technicaltest.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.tecso.technicaltest.service.TokenService;


@Service
public class TokenServiceImpl implements TokenService {

	private final Log logger = LogFactory.getLog(this.getClass());
    public static final String TOKEN_SECRET = "tecsosecrettokengenerate";

    /**
     * 
     */
    public String createToken(UserDetails userDetails) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            String token = JWT.create()
                    .withClaim("userId", userDetails.getUsername().toString())
                    .withClaim("createdAt", new Date())
                    .sign(algorithm);
            return token;
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
            logger.error(exception.getMessage());
        } catch (JWTCreationException exception) {
            exception.printStackTrace();
            logger.error(exception.getMessage());
        }
        return null;
    }

    /**
     * 
     */
    public String getUserIdFromToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt.getClaim("userId").asString();
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
            logger.error(exception.getMessage());
            return null;
        } catch (JWTVerificationException exception) {
            exception.printStackTrace();
            logger.error(exception.getMessage());
            return null;
        }
    }

    public boolean isTokenValid(String token) {
        String userId = this.getUserIdFromToken(token);
        return userId != null;
    }
}