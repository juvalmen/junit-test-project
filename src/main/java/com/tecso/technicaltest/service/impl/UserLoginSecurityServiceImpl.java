package com.tecso.technicaltest.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tecso.technicaltest.repository.jpa.JpaUserLoginRepository;
import com.tecso.technicaltest.service.UserLoginSecurityService;
import com.tecso.technicaltest.util.validations.SecurityValidationMessages;

@Service("userDetailsService")
public class UserLoginSecurityServiceImpl implements UserLoginSecurityService {

	JpaUserLoginRepository jpaUserRepository;
	
	static final String ADMIN = "ADMIN";

	/**
	 * 
	 * @author Julian Valencia
	 * 20/05/2019
	 * @param jpaUserRepository
	 */
	@Autowired
	public UserLoginSecurityServiceImpl(JpaUserLoginRepository jpaUserRepository) {
		this.jpaUserRepository = jpaUserRepository;
	}

	/**
	 * 
	 */
	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

		com.tecso.technicaltest.model.User user = jpaUserRepository.findUserByUsername(username);

		if (user != null) {
			List<GrantedAuthority> authorities = buildUserAuthority(ADMIN);
			return buildUserForAuthentication(user, authorities);
		} else {
			throw new UsernameNotFoundException(SecurityValidationMessages.INVALID_TOKEN);
		}
	}

	/**
	 * 
	 */
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsernameAndToken(final String username, String token)
			throws UsernameNotFoundException {

		com.tecso.technicaltest.model.User user = jpaUserRepository.findUserByUsername(username);

		if (user != null && user.getToken().equals(token)) {
			List<GrantedAuthority> authorities = buildUserAuthority(ADMIN);
			return buildUserForAuthentication(user, authorities);
		} else {
			throw new UsernameNotFoundException(SecurityValidationMessages.INVALID_TOKEN);
		}
	}

	/**
	 * 
	 * @param user
	 * @param authorities
	 * @return
	 */
	private User buildUserForAuthentication(com.tecso.technicaltest.model.User user,
			List<GrantedAuthority> authorities) {
		return new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true, authorities);
	}

	/**
	 * 
	 * @param userRoles
	 * @return
	 */
	private List<GrantedAuthority> buildUserAuthority(String userRoles) {
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		setAuths.add(new SimpleGrantedAuthority(userRoles));

		return new ArrayList<>(setAuths);
	}
}
