package com.tecso.technicaltest.service;

import org.springframework.security.core.userdetails.UserDetails;

public interface TokenService {

    public String createToken(UserDetails userDetails);

    public String getUserIdFromToken(String token);
    
    public boolean isTokenValid(String token);

}