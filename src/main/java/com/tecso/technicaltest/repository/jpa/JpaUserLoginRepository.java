package com.tecso.technicaltest.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tecso.technicaltest.model.User;


public interface JpaUserLoginRepository extends JpaRepository<User, String>{
	
	public User findUserByUsername(String username);
	
	public User findUserByUsernameAndToken(String username,String token);
}
