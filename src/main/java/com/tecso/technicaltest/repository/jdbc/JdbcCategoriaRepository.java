package com.tecso.technicaltest.repository.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tecso.technicaltest.dto.CategoriaDto;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
@Repository
public class JdbcCategoriaRepository {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcCategoriaRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<CategoriaDto> getCategoriasList() throws EmptyResultDataAccessException {
		return jdbcTemplate.query("SELECT idcategoria, nombre, valor FROM categoria cat", 
				new BeanPropertyRowMapper<CategoriaDto>(CategoriaDto.class));
	}

	public List<CategoriaDto> getCategoriaLlave(String key) throws EmptyResultDataAccessException {
		return jdbcTemplate.query("SELECT idcategoria, llave, nombre, valor FROM categoria cat WHERE llave = ?", 
				new Object[] { key },
				new BeanPropertyRowMapper<CategoriaDto>(CategoriaDto.class));
	}
	
}
