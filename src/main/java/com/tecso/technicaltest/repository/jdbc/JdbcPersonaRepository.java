package com.tecso.technicaltest.repository.jdbc;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tecso.technicaltest.dto.PersonaDto;

@Repository
public class JdbcPersonaRepository {

	private JdbcTemplate jdbcTemplate;

	public JdbcPersonaRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	/**
	 * 
	 * @author Julian Valencia
	 * 18/05/2019
	 */
	public List<PersonaDto> getPersonaPorIdentificacionCuit(String cuit) {
		
		return jdbcTemplate.query("SELECT * from persona WHERE cuit = ?", 
				new Object[] { cuit },
				new BeanPropertyRowMapper<PersonaDto>(PersonaDto.class));
	}
	
	/**
	 * 
	 * @author Julian Valencia
	 * 18/05/2019
	 */
	public List<PersonaDto> getPersonasList() {
				
		StringBuilder sql = new StringBuilder();
		sql.append("  SELECT per.idpersona ")
		   .append(" ,p.nombre ")
		   .append(" ,per.apellido ")
		   .append(" ,per.dni ")
		   .append(" ,per.cuit ")
		   .append(" ,per.razonsocial ")
		   .append(" ,per.aniofundacion ")
		   .append(" ,cat.idcatergoria as idCategoriaPersona ")
		   .append(" ,cat.valor as valorCategoriaPersona ")
		   .append(" ,cat.nombre as descripcionCategoriaPersona ")
		   .append(" FROM persona per ")
		   .append(" INNER JOIN categoria cat ON cat.idcategoria = per.categoriapersona; ");
		return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<PersonaDto>(PersonaDto.class));
	}
	
	
/*	
public Integer saveListCamiones(List<Camion> camiones) throws Exception {
		
		Integer numberRecordsSucessful = 0;
		
		try {
			
			for (Camion camion : camiones) {
				StringBuffer sql = new StringBuffer("INSERT INTO pruebakometsales.camion( ");
				sql.append(" placa, ");
				sql.append(" ejes, ");
				sql.append(" capacidad, ");
				sql.append(" cantidadruedas, ");
				sql.append(" color, ");
				sql.append(" tipo, ");
				sql.append(" fechacreacion) ");
				sql.append(" VALUES ");
				sql.append(" (?,?,?,?,?,?,?); ");

				Object[] params = new Object[] { 
						camion.getPlaca(),
						camion.getEjes(),
						camion.getCapacidad(),
						camion.getCantidadruedas(),
						camion.getColor(),
						camion.getTipo(),
						camion.getFechacreacion(),
				};

				int[] types = new int[] { 
						Types.VARCHAR, 
						Types.INTEGER, 
						Types.INTEGER, 
						Types.INTEGER, 
						Types.VARCHAR, 
						Types.VARCHAR, 
						Types.DATE };

				int row = jdbcTemplateObject.update(sql.toString(), params, types);
				numberRecordsSucessful++;
				log.info(row + " row inserted.");
			}
			

	} catch (Exception e) {
		log.error(e.getMessage());
	} 
	
*/	
	
}
