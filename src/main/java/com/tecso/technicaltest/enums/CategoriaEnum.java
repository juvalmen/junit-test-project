package com.tecso.technicaltest.enums;

public enum CategoriaEnum {
	FISICA("10"),
	JURIDICA("20");
	private String type;	
	private CategoriaEnum(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
