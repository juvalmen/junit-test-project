package com.tecso.technicaltest.enums;

public enum AuthorityNameEnum {
    ROLE_USER, ROLE_ADMIN
}