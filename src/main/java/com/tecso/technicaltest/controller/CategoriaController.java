package com.tecso.technicaltest.controller;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tecso.technicaltest.dto.BaseResponseDto;
import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.service.CategoriaService;
import com.tecso.technicaltest.util.SystemMessage;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {
	
	private CategoriaService categoriaService;
	
	public CategoriaController(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@GetMapping(value = "/getByKey", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> getTypeByName(@RequestParam String key) {
		
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		try {
			List<CategoriaDto> categorias = categoriaService.getCategoriaLlave(key);
			response.setResponseMessage(SystemMessage.STATUS_OK);
			response.setResponseBody(categorias);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response,headers,httpStatus);
	}

}
