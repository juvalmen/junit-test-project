-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
-- drop database exerciseone
-- -----------------------------------------------------
-- Schema exerciseonetype
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema exerciseone
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `exerciseone` DEFAULT CHARACTER SET utf8 ;
USE `exerciseone` ;

-- drop database exerciseone

-- -----------------------------------------------------
-- Table `exerciseone`.`type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exerciseone`.`type` (
  `idtype` int(11) NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(20) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `value`  VARCHAR(4) NOT NULL,
  PRIMARY KEY (`idtype`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `exerciseone`.`person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exerciseone`.`person` (
  `idperson` int(11) NOT NULL AUTO_INCREMENT,
  `dni` VARCHAR(45) NULL,
  `cuit` VARCHAR(45) NOT NULL,
  `name` VARCHAR(80) NULL,
  `last_name` VARCHAR(250) NULL,
  `business_name` VARCHAR(100) NULL,
  `foundation_year` INT NULL,
  `typeperson` INT NULL,
  PRIMARY KEY (`idperson`),
  UNIQUE INDEX `cuit_UNIQUE` (`cuit` ASC) VISIBLE,
  INDEX `fk_person_idx` (`typeperson` ASC) VISIBLE,
  CONSTRAINT `fk_person_idtype_type`
    FOREIGN KEY (`typeperson`)
    REFERENCES `exerciseone`.`type` (`idtype`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- DROP TABLE IF EXISTS user ;
CREATE TABLE IF NOT EXISTS user (
  id int(11) NOT NULL AUTO_INCREMENT,
  username VARCHAR(45) NOT NULL,
  password VARCHAR(60) NOT NULL,
  enabled SMALLINT NOT NULL,
  token VARCHAR(60) NULL,
  PRIMARY KEY (id));


INSERT INTO `exerciseone`.`type`(`idtype`,`key`,`name`,`value`) VALUES(1,'tipopersona','Fisica','10');
INSERT INTO `exerciseone`.`type`(`idtype`,`key`,`name`,`value`) VALUES(2,'tipopersona','Juridica','20');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
